def count_letter(letter: str, letterzzo_stringz: str) -> int:
    return sum(map(lambda letter_string: 1 if letter in letter_string else 0, letter_string))

no_of_letters = input()
letter_string = input()
print(count_letter('n', letter_string) * "1 " + count_letter('z', letter_string) * "0 ")

